import React from 'react';
import {Link,Route,BrowserRouter as Router} from "react-router-dom"
import Homepage from './components/Homepage'
import Fallceling from './components/fallceiling'
import Flooring from './components/flooring'
import Wallpaper from './components/Wallpaper'
import Woodenworks from './components/woodenworks'
import './App.css';

function App() {

  return (
    <Router>
 <h1 align="center"><Link to="/Homepage">Sai Interior</Link> <br /> </h1>
<Route exact path="/Homepage"><Homepage/></Route>
<Route path="/Wallpaper"><Wallpaper/></Route>
<Route path="/FallCeling"><Fallceling/></Route>
<Route path="/Flooring"><Flooring /></Route> 
<Route path="/WoodenWorks"><Woodenworks /></Route>
    </Router>
  );
}

export default App;
